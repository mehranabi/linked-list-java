package abi.mehran;

public class LinkedList<T> {
    private Node<T> start = null;

    /**
     * Print the list
     */
    public void print() {
        Node<T> node = this.start;
        while (node != null) {
            System.out.println(node.data);
            node = node.next;
        }
    }

    /**
     * Add new item at the end of the list
     *
     * @param data Data of new node
     */
    public void insert(T data) {
        if (this.start == null) {
            this.start = new Node<>(data);
            return;
        }

        Node<T> newNode = new Node<>(data);
        Node<T> lastNode = this.lastNode();
        lastNode.next = newNode;
    }

    /**
     * Insert a new node after another node
     *
     * @param data  Data of new node
     * @param after Data of the node that the new node will be inserted after it
     * @return If the new node has been inserted
     */
    public boolean insertAfter(T data, T after) {
        boolean inserted = false;

        Node<T> newNode = new Node<>(data);
        Node<T> node = this.start;

        while (node != null) {
            if (node.data == after) {
                newNode.next = node.next;
                node.next = newNode;
                inserted = true;
                break;
            }
            node = node.next;
        }

        return inserted;
    }

    /**
     * Merge another list to the current list
     *
     * @param list A list to be merged with current list
     */
    public void merge(LinkedList<T> list) {
        Node<T> lastNode = this.lastNode();
        lastNode.next = list.firstNode();
    }

    /**
     * Delete the list
     */
    public void deleteList() {
        this.start = null;
    }

    /**
     * Delete a node
     *
     * @param data Data of the node to be delete
     * @return If the node has been deleted
     */
    public boolean deleteNode(T data) {
        if (this.start != null && this.start.data == data) {
            this.start = null;
            return true;
        }

        boolean deleted = false;

        Node<T> prev = this.start;
        Node<T> node = this.start.next;

        while (node != null) {
            if (node.data == data) {
                prev.next = node.next;
                deleted = true;
                break;
            }
            prev = node;
            node = node.next;
        }

        return deleted;
    }

    /**
     * Reverse items in the list
     */
    public void reverse() {
        // TODO: Implement reversing the list
    }

    /**
     * Check if list is empty
     *
     * @return If list is empty
     */
    public boolean isEmpty() {
        return this.start == null;
    }

    /**
     * Get first node in the list
     *
     * @return List's first node
     */
    public Node<T> firstNode() {
        return this.start;
    }

    /**
     * Find the last node in the list
     *
     * @return Last node in the list
     */
    public Node<T> lastNode() {
        Node<T> node = this.start;
        while (node.next != null) {
            node = node.next;
        }
        return node;
    }
}
